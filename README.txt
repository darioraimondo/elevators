EJERCICIO DE LOGICA

“Elevadores inteligentes”

Se debe programar un software para gestionar 3 elevadores de un edificio de 20 pisos, que desea ahorrar lo máximo posible en energía. Los elevadores están disponibles a los pasajeros, para tomarlos o dejarlos en todos los pisos. Hay un solo botón de llamada de ascensor por piso.

Qué variables tendría en cuenta? Realice una función con esas variables, que resuelva qué ascensor debe responder un llamado en un momento dado. Dé ejemplos de cómo sería la lógica si llaman desde planta baja, último piso y otro piso cualquiera, aclarando para cada caso el estado de todos los ascensores. 


La función que implementa la lógica del manejo de ascensores pueden encontrarla en el archivo:
    • ElevatorSelectionService.java
    • Metodo: getLastCallAndAssignElevator
    • ubicado en el path: src/main/java/com/example/demo/services/

Adicionalmente los ejemplos de llamadas del ascensor desde diferentes pisos y contemplando diferentes escenarios se pueden ver en el archivo:
    • ElevatorSelectionServiceTest.java
    • ubicado en el path: src/test/java/com/example/demo/


