package com.example.demo;


import com.example.demo.models.Elevator;
import com.example.demo.services.ElevatorCallingService;
import com.example.demo.services.ElevatorSelectionService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;


@Slf4j
@SpringBootTest
public class ElevatorSelectionServiceTest {

	@Autowired
	ElevatorCallingService elevatorCallingService;

	@Autowired
	ElevatorSelectionService elevatorSelectionService;


	@BeforeAll
	static void init(){
		log.info("init -> set up once");
	}


	private List<Elevator> getElevatorListMock(){
		List<Elevator> elevatorListMock = new ArrayList<>();
		elevatorListMock.add(new Elevator("Elevator 1"));
		elevatorListMock.add(new Elevator("Elevator 2"));
		elevatorListMock.add(new Elevator("Elevator 3"));

		return elevatorListMock;
	}

	/**
	 * the caller is standing in floor 5
	 * all elevators ready and in ground floor, the caller is in floor 5, the system chooses elevator 1
	 * Elevator 1: currentFloor=0, isInMaintenance = false, isFull = false, movingUp = false, movingDown = false
	 * Elevator 2: same
	 * Elevator 3: same
	 * */

	@Test
	void callElevatorFromGroundFloorTest(){
		log.info("************************callElevatorFromGroundFloorTest************************");
		elevatorSelectionService.setElevatorList(getElevatorListMock());

		elevatorCallingService.callElevatorFromFloorNumber(5);//producer -> elevators being called
		Elevator elevator = elevatorSelectionService.getLastCallAndAssignElevator();//consumer -> elevator being assigned

		log.info("Elevator assigned: "+elevator);
		Assertions.assertEquals(elevator.getElevatorName(), "Elevator 1", "Chosen elevator: ");
		log.info("*******************************************************************************");
	}

	/**
	 * the caller is standing in floor 20
	 * all elevators are working , and the closest one is the elevator 3 which is in floor 19
	 * Elevator 1: currentFloor=0, isInMaintenance = false, isFull = false, movingUp = false, movingDown = false
	 * Elevator 2: currentFloor=10, isInMaintenance = false, isFull = false, movingUp = false, movingDown = false
	 * Elevator 3: currentFloor=19, isInMaintenance = false, isFull = false, movingUp = false, movingDown = false
	 */
	@Test
	void callElevatorFromRoofTopTest(){
		log.info("************************callElevatorFromRoofTopTest****************************");
		List<Elevator> elevatorListMock = getElevatorListMock();
		elevatorListMock.get(0).setCurrentFloor(0);
		elevatorListMock.get(1).setCurrentFloor(10);
		elevatorListMock.get(2).setCurrentFloor(19);
		elevatorSelectionService.setElevatorList(elevatorListMock);

		elevatorCallingService.callElevatorFromFloorNumber(20);
		Elevator elevator = elevatorSelectionService.getLastCallAndAssignElevator();

		log.info("Elevator assigned: "+elevator);
		Assertions.assertEquals(elevator.getElevatorName(), "Elevator 3", "Chosen elevator: ");
		log.info("*******************************************************************************");
	}

	/**
	 * the caller is standing in floor 20
	 * elevator 2 is moving away from caller and elevator 3 in maintenance.
	 * the only remaining elevator available is 1, which is far away, but still eligible
	 * Elevator 1: currentFloor=0, isInMaintenance = false, isFull = false, movingUp = false, movingDown = false
	 * Elevator 2: currentFloor=10, isInMaintenance = true, isFull = false, movingUp = false, movingDown = false
	 * Elevator 3: currentFloor=19, isInMaintenance = false, isFull = false, movingUp = false, movingDown = true
	 *
	 */
	@Test
	void callElevatorButInMaintenanceAndMoving(){
		log.info("************************callElevatorButInMaintenanceAndMoving******************");
		List<Elevator> elevatorListMock = getElevatorListMock();
		elevatorListMock.get(0).setCurrentFloor(0);
		elevatorListMock.get(1).setCurrentFloor(10);
		elevatorListMock.get(1).setInMaintenance(true);
		elevatorListMock.get(2).setCurrentFloor(19);
		elevatorListMock.get(2).setMovingDown(true);
		elevatorSelectionService.setElevatorList(elevatorListMock);

		elevatorCallingService.callElevatorFromFloorNumber(20);
		Elevator elevator = elevatorSelectionService.getLastCallAndAssignElevator();

		log.info("Elevator assigned: "+elevator);
		Assertions.assertEquals(elevator.getElevatorName(), "Elevator 1", "Chosen elevator: ");
		log.info("*******************************************************************************");
	}

}
