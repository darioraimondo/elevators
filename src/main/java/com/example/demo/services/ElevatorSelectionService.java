package com.example.demo.services;

import com.example.demo.models.Elevator;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@ToString
public class ElevatorSelectionService {

    /**
     * EJERCICIO DE LOGICA
     *
     * “Elevadores inteligentes”
     *
     * Se debe programar un software para gestionar 3 elevadores de un edificio de 20 pisos,
     * que desea ahorrar lo máximo posible en energía. Los elevadores están disponibles a los pasajeros,
     * para tomarlos o dejarlos en todos los pisos. Hay un solo botón de llamada de ascensor por piso.
     *
     * Qué variables tendría en cuenta?
     * Realice una función con esas variables, que resuelva qué ascensor debe responder un llamado en un momento dado.
     * Dé ejemplos de cómo sería la lógica si llaman desde planta baja,
     * último piso y otro piso cualquiera,
     * aclarando para cada caso el estado de todos los ascensores.
     *
     * piso inicial del ascensor
     * piso del cual lo llaman
     * distancia al piso
     * posicion actual
     * movimiento hacia arriba o abajo
     * mantenimiento
     */

    @Autowired
    private ElevatorQueueService elevatorQueueService;


    private List<Elevator> elevatorList = new ArrayList<>();

    public void setElevatorList(List<Elevator> elevators){
        this.elevatorList = elevators;
    }


/***
 * 1. read the queue of elevator calls and takes the first call
 * 2. searches for all available elevators
 * 3. returns the closest one the the caller.
 */
    public Elevator getLastCallAndAssignElevator(){

        List<Elevator> availableElevators;

        //get first call of the queue, an improvement would be to take solve all calls
        //using elevatorQueueService.getQueue().drainTo() but for the example the simpler was chosen.
        int floorNumber = elevatorQueueService.getQueue().remove();

        availableElevators = getAvailableElevators(floorNumber);

        if (availableElevators.isEmpty()) {
            log.info("there is no available elevator");
            return null;
        }

        return chooseElevatorFromAvailable(availableElevators, floorNumber);
    }


    private List<Elevator> getAvailableElevators(int callingFloorNumber){

        List<Elevator> availableElevators = new ArrayList<>();

        //check if full, in maintenance or moving towards caller

        for(Elevator elevator : elevatorList){
            if( !elevator.isFull() &&
                    !elevator.isInMaintenance() &&
                    elevator.isMovingTowardsCaller(callingFloorNumber)){
                log.info("the elevator "+elevator.getElevatorName()+" meets all conditions");
                availableElevators.add(elevator);
            }
        }
        return availableElevators;
    }

    private Elevator chooseElevatorFromAvailable(List<Elevator> availableElevators, int floorNumber){
        //choose closest available elevator
        Elevator elevatorToReturn = null;
        int closestElevatorDistanceToFloor = Integer.MAX_VALUE;
        int currentElevatorDistanceToFloor;

        for (Elevator availableElevator : availableElevators) {
            currentElevatorDistanceToFloor = availableElevator.getDistanceToFloor(floorNumber);
            log.info("the elevator "+availableElevator.getElevatorName()+" is "+currentElevatorDistanceToFloor+" floors away");
            if (currentElevatorDistanceToFloor < closestElevatorDistanceToFloor) {
                closestElevatorDistanceToFloor = currentElevatorDistanceToFloor;
                elevatorToReturn = availableElevator;
            }
        }

        return elevatorToReturn;
    }

}
