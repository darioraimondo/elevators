package com.example.demo.services;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
@Service
@ToString
@Getter
@Setter
public class ElevatorQueueService {

    private BlockingQueue<Integer> queue;

    public ElevatorQueueService(){
         this.queue = new LinkedBlockingQueue<>();
    }

}
