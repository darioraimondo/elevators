package com.example.demo.services;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@ToString
@Getter
@Setter
@NoArgsConstructor
public class ElevatorCallingService {

    @Autowired
    private ElevatorQueueService elevatorQueueService;


    public void callElevatorFromFloorNumber(int floorNumber){

        try {
            elevatorQueueService.getQueue().put(floorNumber);

        } catch (InterruptedException e) {
            log.error("Something went wrong with the Queue: "+e.getMessage());
            e.printStackTrace();
        }

    }


}
