package com.example.demo.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@ToString
@Getter
@Setter
public class Elevator {
    private String elevatorName;
    private int currentFloor = 0;
    private boolean movingUp = false;
    private boolean movingDown = false;
    private boolean isFull = false;
    private boolean isInMaintenance = false;

    public Elevator(String elevatorName){
        this.elevatorName = elevatorName;
    }

    public boolean isMovingTowardsCaller(int callerFloorNumber){

        if(callerFloorNumber > getCurrentFloor() && !isMovingDown())
            return true;

        if(callerFloorNumber < getCurrentFloor() && !isMovingUp())
            return true;

        return false;
    }

    public int getDistanceToFloor(int callerFloorNumber){
        return Math.abs(getCurrentFloor() - callerFloorNumber);
    }

}
